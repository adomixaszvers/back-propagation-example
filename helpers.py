import sys

__author__ = 'adomas'


def make_matrix(rows, cols):
    return [[0.0 for _ in range(cols)] for _ in range(rows)]


def show_vector(vector, decimals, blank_line):
    format_str = "%%.%df" % decimals
    for i in range(len(vector)):
        if i > 0 and i % 12 == 0:
            print()
        if vector[i] >= 0.0:
            print(end=" ")
        print(format_str % vector[i], end=' ')
    if blank_line:
        print("\n")


def show_matrix(matrix, num_rows, decimals):
    format_str = "%%.%df" % decimals
    if num_rows == -1:
        num_rows = sys.maxsize
    for i in range(min(len(matrix), num_rows)):
        for j in range(len(matrix[i])):
            if matrix[i][j] >= 0.0:
                print(end=" ")
            print(format_str % matrix[i][j], end=' ')
        print()
    print()


def make_vector(length):
    return [0.0 for _ in range(length)]
