import helpers
import math

__author__ = 'adomas'


class NeuralNetwork(object):
    def __init__(self, num_input, num_hidden, num_output):
        self.num_input = num_input
        self.num_hidden = num_hidden
        self.num_ouput = num_output

        self.inputs = helpers.make_vector(num_input)
        self.ih_weights = helpers.make_matrix(num_input, num_hidden)
        self.ih_sums = helpers.make_vector(num_hidden)
        self.ih_biases = helpers.make_vector(num_hidden)
        self.ih_outputs = helpers.make_vector(num_hidden)
        self.ho_weights = helpers.make_matrix(num_hidden, num_output)
        self.ho_sums = helpers.make_vector(num_output)
        self.ho_biases = helpers.make_vector(num_output)
        self.outputs = helpers.make_vector(num_output)

        self.o_grads = helpers.make_vector(num_output)
        self.h_grads = helpers.make_vector(num_hidden)

        self.ih_prev_weights_delta = helpers.make_matrix(num_input, num_hidden)
        self.ih_prev_biases_delta = helpers.make_vector(num_hidden)
        self.ho_prev_weights_delta = helpers.make_matrix(num_hidden, num_output)
        self.ho_prev_biases_delta = helpers.make_vector(num_output)

    def update_weights(self, t_values, eta, alpha):
        """update the weights and biases using back-propagation, with target values,
        eta (learning rate), alpha (momentum)"""

        # assumes that SetWeights and ComputeOutputs have been called and so all
        # the internal arrays and matrices have values (other than 0.0)
        if len(t_values) != self.num_ouput:
            raise Exception("target values not same Length as output in UpdateWeights")

        # 1. compute output gradients
        for i in range(len(self.o_grads)):
            derivative = (1 - self.outputs[i]) * (1 + self.outputs[i])  # derivative of tanh
            self.o_grads[i] = derivative * (t_values[i] - self.outputs[i])

        # 2. compute hidden gradients
        for i in range(len(self.h_grads)):
            # (1 / 1 + exp(-x))'  -- using output value of neuron
            derivative = (1 - self.ih_outputs[i]) * self.ih_outputs[i]
            sum_ = 0.0
            for j in range(self.num_ouput):  # each hidden delta is the sum of numOutput terms
                sum_ += self.o_grads[j] * self.ho_weights[i][j]  # each downstream gradient * outgoing weight
            self.h_grads[i] = derivative * sum_

        # 3. update input to hidden weights (gradients must be computed right-to-left
        # but weights can be updated in any order
        for i in range(len(self.ih_weights)):  # 0..2 (3)
            for j in range(len(self.ih_weights[0])):  # 0..3 (4)
                delta = eta * self.h_grads[j] * self.inputs[i]  # conpute the new delta
                self.ih_weights[i][j] += delta  # update
                # add momentum using previous delta. on first pass old value will be 0.0 but that's OK.
                self.ih_weights[i][j] += + alpha * self.ih_prev_weights_delta[i][j]

        # 3b. update input to hidden biases
        for i in range(len(self.ih_biases)):
            delta = eta * self.h_grads[i] * 1.0  # the 1.0 is the constant input for any bias; could leave out
            self.ih_biases[i] += + delta
            self.ih_biases[i] += + alpha * self.ih_prev_biases_delta[i]

        # 4. update hidden to output weights
        for i in range(len(self.ho_weights)):  # 0..3 (4)
            for j in range(len(self.ho_weights[0])):  # 0..1 (2)
                delta = eta * self.o_grads[j] * self.ih_outputs[i]  # see above: ihOutputs are inputs to next layer
                self.ho_weights[i][j] += + delta
                self.ho_weights[i][j] += alpha * self.ho_prev_weights_delta[i][j]
                self.ho_prev_weights_delta[i][j] = delta

        # 4b.update hidden to output biases
        for i in range(len(self.ho_biases)):
            delta = eta * self.o_grads[i] * 1.0
            self.ho_biases[i] += + delta
            self.ho_biases[i] += + alpha * self.ho_prev_biases_delta[i]
            self.ho_prev_biases_delta[i] = delta

    def set_weights(self, weights):
        """copy weights and biases in weights[] array to i-h weights, i-h biases, h-o weights, h-o biases
        """
        num_weights = (self.num_input * self.num_hidden) +\
                      (self.num_hidden * self.num_ouput) + self.num_hidden + self.num_ouput
        if len(weights) != num_weights:
            raise Exception("The weights array length: " + str(len(weights)) +
                            " does not match the total number of weights and biases: " + str(num_weights))

        k = 0  # points into weights param

        for i in range(self.num_input):
            for j in range(self.num_hidden):
                self.ih_weights[i][j] = weights[k]
                k += 1

        for i in range(self.num_hidden):
            self.ih_biases[i] = weights[k]
            k += 1

        for i in range(self.num_hidden):
            for j in range(self.num_ouput):
                self.ho_weights[i][j] = weights[k]
                k += 1

        for i in range(self.num_ouput):
            self.ho_biases[i] = weights[k]
            k += 1

    def get_weights(self):
        num_weights = (self.num_input * self.num_hidden) + (self.num_hidden * self.num_ouput)\
                     + self.num_hidden + self.num_ouput
        result = helpers.make_vector(num_weights)
        k = 0
        for i in range(len(self.ih_weights)):
            for j in range(len(self.ih_weights[0])):
                result[k] = self.ih_weights[i][j]
                k += 1
        for i in range(len(self.ih_biases)):
            result[k] = self.ih_biases[i]
            k += 1
        for i in range(len(self.ho_weights)):
            for j in range(len(self.ho_weights[0])):
                result[k] = self.ho_weights[i][j]
                k += 1
        for i in range(len(self.ho_biases)):
            result[k] = self.ho_biases[i]
            k += 1
        return result

    def compute_outputs(self, x_values):
        if len(x_values) != self.num_input:
            raise Exception("Inputs array length " + str(len(self.inputs)) +
                            " does not match NN numInput value " + str(self.num_input))

        for i in range(self.num_hidden):
            self.ih_sums[i] = 0.0
        for i in range(self.num_ouput):
            self.ho_sums[i] = 0.0

        for i in range(len(x_values)):  # copy x-values to inputs
            self.inputs[i] = x_values[i]

        for j in range(self.num_hidden):  # compute input-to-hidden weighted sums
            for i in range(self.num_input):
                self.ih_sums[j] += self.inputs[i] * self.ih_weights[i][j]

        for i in range(self.num_hidden):  # add biases to input-to-hidden sums
            self.ih_sums[i] += self.ih_biases[i]

        for i in range(self.num_hidden):  # determine input-to-hidden output
            self.ih_outputs[i] = self.sigmoid_function(self.ih_sums[i])

        for j in range(self.num_ouput):  # compute hidden-to-output weighted sums
            for i in range(self.num_hidden):
                self.ho_sums[j] += self.ih_outputs[i] * self.ho_weights[i][j]

        for i in range(self.num_ouput):  # add biases to input-to-hidden sums
            self.ho_sums[i] += self.ho_biases[i]

        for i in range(self.num_ouput):  # determine hidden-to-output result
            self.outputs[i] = self.hyper_tan_function(self.ho_sums[i])

        result = self.outputs[:]

        return result

    @staticmethod
    def step_function(x):
        if x > 0.0:
            return 1.0
        else:
            return 0.0

    @staticmethod
    def sigmoid_function(x):
        if x < -45.0:
            return 0.0
        elif x > 45.0:
            return 1.0
        else:
            return 1.0 / (1.0 + math.exp(-x))

    @staticmethod
    def hyper_tan_function(x):
        if x < -10.0:
            return -1.0
        elif x > 10:
            return 1.0
        else:
            return math.tanh(x)
