from neuralnetwork import NeuralNetwork
import helpers


__author__ = 'adomas'


def main():
    print("\nBegin Neural Network Back-Propagation demo\n")

    print("Creating a 3-input, 4-hidden, 2-output neural network")

    print("Using sigmoid function for input-to-hidden activation")

    print("Using tanh function for hidden-to-output activation")

    nn = NeuralNetwork(3, 4, 2)

    weights = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2,
               -2.0, -6.0, -1.0, -7.0,
               1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0,
               -2.5, -5.0]

    print("\nInitial 26 random weights and biases are:")
    helpers.show_vector(weights, 2, True)

    print("Loading neural network weights and biases")
    nn.set_weights(weights)

    print("\nSetting inputs:")
    x_values = [1.0, 2.0, 3.0]
    helpers.show_vector(x_values, 2, True)

    initial_outputs = nn.compute_outputs(x_values)
    print("Initial outputs:")
    helpers.show_vector(initial_outputs, 4, True)

    # target (desired) values. note these only make sense for tanh output activation
    t_values = [-0.8500, 0.7500]
    print("Target outputs to learn are:")
    helpers.show_vector(t_values, 4, True)

    # learning rate - controls the maginitude of the increase in the change in weights. found by trial and error.
    eta = 0.90
    # momentum - to discourage oscillation. found by trial and error.
    alpha = 0.04
    print("Setting learning rate (eta) = %.2f and momentum (alpha) = %.2f" % (eta, alpha))

    print("\nEntering main back-propagation compute-update cycle")
    print("Stopping when sum absolute error <= 0.01 or 1,000 iterations\n")
    ctx = 0
    y_values = nn.compute_outputs(x_values)  # prime the back-propagation loop
    error = error_f(t_values, y_values)
    while ctx < 1000 and error > 0.01:
        print("===================================================")
        print("iteration = %d" % ctx)
        print("Updating weights and biases using back-propagation")
        nn.update_weights(t_values, eta, alpha)
        print("Computing new outputs:")
        y_values = nn.compute_outputs(x_values)
        helpers.show_vector(y_values, 4, False)
        print("\nComputing new error")
        error = error_f(t_values, y_values)
        print("Error = %.4f" % error)
        ctx += 1
    print("===================================================")

    print("\nBest weights and biases found:")
    best_weights = nn.get_weights()
    helpers.show_vector(best_weights, 2, True)

    print("End Neural Network Back-Propagation demo\n")


def error_f(target, output):
    """sum absolute error. could put into NeuralNetwork class."""
    sum_ = 0.0
    for i in range(len(target)):
        sum_ += abs(target[i] - output[i])
    return sum_


if __name__ == "__main__":
    main()
